# Update Server for Pycom IOT

This is a server for providing OTA software updates to Pycom devices.  
It is written in PHP on the [Laravel Lumen](https://lumen.laravel.com) microframework.

The OTA class will make a `GET` request to `{your-server}/manifest.json?current_ver={current_version}`  
The output will be something like the following:

```
{
   "delete": [
      "flash/old_file.py",
      "flash/other_old_file.py"
   ],
   "firmware": {
       "URL": "http://192.168.1.144:8000/firmware_1.0.1b.bin",
       "hash": "ccc6914a457eb4af8855ec02f6909316526bdd08"
   },
   "new": [
       {
           "URL": "http://192.168.1.144:8000/1.0.1b/flash/lib/new_lib.py",
           "dst_path": "flash/lib/new_lib.py",
           "hash": "1095df8213aac2983efd68dba9420c8efc9c7c4a"
       }
   ],
   "update": [
       {
           "URL": "http://192.168.1.144:8000/1.0.1b/flash/changed_file.py",
           "dst_path": "flash/changed_file.py",
           "hash": "1095df8213aac2983efd68dba9420c8efc9c7c4a"
       }
   ],
   "version": "1.0.1b"
}
```

This follows the output defined by pycom here: https://docs.pycom.io/chapter/tutorials/all/ota.html?q=#how-to-use

## Versioning your software

This server makes versioning your firmware very easy: git tags.
Releasing a new version, just tag it using the [semver](https://semver.org) versioning convention.  

## Setup

To setup the server:

Use `Composer` to install the server:

`composer create-project tobz/pycom-update-server`

During installation you will be asked for the repository of your pycom firmware  
Or you can manually run `php artisan firmware:clone`

Make sure your virtualhost's root is pointed to the projects `public` folder, and your good to go.

You will also need to update your firmware repo. You can either work out your own method of doing this (on push for example), or you can run the scheduled command provided by setting up a cron job.

```bash
* * * * * cd /path-to-update-server && php artisan schedule:run >> /dev/null 2>&1
```

This will pull firmware updates every hour, on the hour.

## To do

[x] Simplify setup  
[ ] Improve documentation  
[ ] Add some authentication. This is reliant on Pycom supporting some sort of auth as well.  
