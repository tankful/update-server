<?php

return [
    'repository' => env('GIT_REPOSITORY'),
    'working_copy' => env('GIT_WORKING_COPY', storage_path('working_copy')),
    'binary' => env('GIT_BINARY', '/usr/bin/git'),
    'home' => env('GIT_HOME', '~/'),
];
