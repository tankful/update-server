<?php

namespace App\Providers;

use GitWrapper\GitWrapper;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function boot()
    {
        // load main config file
        $this->app->configure('app');
        $this->app->configure('git');

        $this->app->singleton('git', function ($app) {
            $git = new GitWrapper(config('git.binary'));
            $git->setEnvVar('HOME', config('git.home'));

            return $git->workingCopy(config('git.working_copy'));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
