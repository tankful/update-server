<?php

namespace App\Http\Controllers;

use GitWrapper\GitCommand;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeExtensionGuesser;
use Symfony\Component\HttpFoundation\Response as BaseResponse;

class ManifestController extends Controller
{
    /**
     * Firmware Manifest
     * Output as required by pycom for OTA updates.
     * See https://docs.pycom.io/chapter/tutorials/all/ota.html#how-to-use for details.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request): BaseResponse
    {
        $tags = implode(',', $this->getTags());
        $this->validate($request, [
            'current_ver' => "required|regex:/\d+\.\d+\.\d+/|in:{$tags}"
        ], [
            'required' => 'current_ver is required',
            'regex' => 'Must be in the \'semver\' format',
            'in' => 'Invalid version'
        ]);

        if ($this->needsUpdate($request->current_ver) === false) {
            return response('null', 200);
        }

        return app('cache')->remember(
            $request->input('current_ver'),
            config('app.cache_time'),
            function () use ($request) {
                return response()->json($this->diff($request->current_ver));
            }
        );
    }

    /**
     * Download a file from git at the given tag/commit
     *
     * @param  \Illuminate\Http\Request $request
     * @param  string  $version
     * @param  string  $path
     * @return \Illuminate\Http\Response
     */
    public function file(Request $request, string $version, string $path): Response
    {
        $mime = (new class extends MimeTypeExtensionGuesser {
            public function guessMime($ext)
            {
                $this->defaultExtensions['text/x-python'] = 'py';
                return array_search($ext, $this->defaultExtensions);
            }
        })->guessMime(preg_replace('/.+\.(.+$)/', '$1', $path)) ?: 'text/plain';

        return response($this->getFileContent($version, $path), 200, [
            'Content-Type' => $mime,
            'Content-Disposition' => 'attachment',
        ]);
    }

    /**
     * Get the current firmware version
     *
     * @param Request $request
     * @return void
     */
    public function currentVersion(Request $request)
    {
        return [
            'current_version' => current($this->getTags()),
        ];
    }

    /**
     * Check if the version requires an update
     *
     * @param  string $version
     * @param  string $operator
     * @return bool
     */
    private function needsUpdate(string $version, string $operator = '>'): bool
    {
        foreach ($this->getTags() as $tag) {
            if (version_compare($tag, $version, $operator)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get all the tags from the git repository
     *
     * @return array
     */
    private function getTags(): array
    {
        $gitTags = app('git')->run('tag', ['-l']);
        $tagsArray = explode("\n", trim($gitTags, "\n"));
        rsort($tagsArray, SORT_NATURAL);

        return $tagsArray;
    }

    /**
     * Get the contents of a file from a speficied tag/commit
     *
     * @param  string $version
     * @param  string $path
     * @return string
     */
    private function getFileContent(string $version, string $path): string
    {
        $fileTagPath = vsprintf('%s:%s', compact('version', 'path'));
        return app('git')->run('cat-file', ['-p', $fileTagPath]);
    }

    /**
     * Compute the final update report
     *
     * @param  string $version
     * @return array
     */
    private function diff(string $version): array
    {
        $tags = $this->getTags();
        $latestVersion = current($tags);
        $diffString = app('git')->run('diff', [
            $version,
            $latestVersion,
            '--name-status'
        ]);
        $diff = explode("\n", trim($diffString, "\n"));

        $output = [
            'delete' => [],
            'update' => [],
            'new' => [],
            'version' => $latestVersion,
            'previous_version' => $version,
        ];

        foreach ($diff as $fileDiff) {
            // get type of change
            $diffType = substr($fileDiff, 0, 1);
            $file = trim(substr($fileDiff, 1));

            if (substr($file, 0, 1) == '.') {
                // ignore hidden files
                continue;
            }

            // is the file a bin file (Pycom firmware)?
            // push that and move on
            if (preg_match('/.+\.bin$/', $file)) {
                $output['firmware'] = $this->addFile($file, $latestVersion);
                continue;
            }

            switch ($diffType) {
                case 'D':
                    array_push($output['delete'], sprintf('/flash/%s', $file));
                    break;
                case 'M':
                    array_push($output['update'], $this->addFile($file, $latestVersion));
                    break;
                case 'A':
                    array_push($output['new'], $this->addFile($file, $latestVersion));
                    break;
            }
        }

        return $output;
    }

    /**
     * Build file data array reay to add to final output
     *
     * @param string $file
     * @param string $latestVersion
     * @return  array
     */
    private function addFile(string $file, string $latestVersion): array
    {
        return [
            'URL' => route('file', ['version' => $latestVersion, 'path' => $file]),
            'dest_path' => sprintf('/flash/%s', $file),
            'hash' => sha1($this->getFileContent($latestVersion, $file)),
        ];
    }
}
