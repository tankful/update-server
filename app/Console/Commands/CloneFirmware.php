<?php

namespace App\Console\Commands;

use App\Device;
use App\Notifications\DeviceMissingNotification;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use RecursiveIteratorIterator;
use Symfony\Component\Finder\Iterator\RecursiveDirectoryIterator;

class CloneFirmware extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'firmware:clone {--F|force : Delete existing working copy before cloning}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Setup repository server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $repository = $this->ask('Repository:', config('git.repository'));
        if (empty(config('git.repository')) || $repository != config('git.repository')) {
            $this->setVar('GIT_REPOSITORY', $repository);
            config(['git.repository' => $repository]);
        }

        $workingCopy = $this->ask('Clone to:', config('git.working_copy'));
        if ($workingCopy !== config('git.working_copy')) {
            $this->setVar('GIT_WORKING_COPY', $workingCopy);
            config(['git.working_copy' => $workingCopy]);
        }

        if (file_exists($workingCopy) && count(scandir($workingCopy))) {
            if ($this->option('force') || $this->confirm('Delete existing working copy?', true)) {
                $this->comment('Deleting existing working copy...');
                $this->rmdir($workingCopy);
                $this->info('Done.');
            } else {
                return $this->error('Working Copy already exists and is not empty');
            }
        }

        try {
            $this->comment('Cloning...');
            app('git')->cloneRepository(config('git.repository'));
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }

        $this->info('Setup Complete');
    }

    /**
     * Write Environment variable to disk
     *
     * @param string $key
     * @param string $value
     * @return void
     */
    protected function setVar($key, $value): void
    {
        $envFilePath = base_path('.env');
        $contents = file_get_contents($envFilePath);

        if (($oldValue = $this->getOldValue($contents, $key)) !== null) {
            $contents = str_replace("{$key}={$oldValue}", "{$key}={$value}", $contents);
            $this->writeFile($envFilePath, $contents);
        } else {
            $contents = $contents . "\n{$key}={$value}\n";
            $this->writeFile($envFilePath, $contents);
        }
    }

    /**
     * Get the old value of a given key from an environment file.
     *
     * @param string $envFile
     * @param string $key
     * @return string
     */
    protected function getOldValue(string $envFile, string $key): ?string
    {
        // Match the given key at the beginning of a line
        preg_match("/^{$key}=[^\r\n]*/uim", $envFile, $matches);
        if (count($matches)) {
            return substr($matches[0], strlen($key) + 1);
        }
        return null;
    }

    /**
     * Overwrite the contents of a file.
     *
     * @param string $path
     * @param string $contents
     * @return boolean
     */
    protected function writeFile(string $path, string $contents): bool
    {
        $file = fopen($path, 'w');
        fwrite($file, $contents);
        return fclose($file);
    }

    /**
     * Recursively remove directory
     *
     * @param string $dir
     * @return void
     */
    protected function rmdir($dir)
    {
        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS),
            RecursiveIteratorIterator::CHILD_FIRST
        );

        foreach ($files as $fileinfo) {
            $action = ($fileinfo->isDir() ? 'rmdir' : 'unlink');
            $action($fileinfo->getRealPath());
        }
    }
}
