<?php

namespace App\Console\Commands;

use App\Device;
use App\Notifications\DeviceMissingNotification;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class UpdateFirmware extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'firmware:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the firmware repository';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        app('git')->getWrapper()->streamOutput();
        app('git')->pull();
    }
}
